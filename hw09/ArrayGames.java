import java.util.Random;
//import java.util.Array;
import java.util.Scanner;

public class ArrayGames{
  public static void main(String args[]){
    
    Scanner MyScanner= new Scanner(System.in);// declaring scanner
    System.out.print("Do you want to run insert or shorten?: "); //prompting user to pick which method
    
    //declaring variables
    String resp = MyScanner.next();
    String i= "insert";
    String s ="shorten";
    int arrMem1;
    int arrMem2;
    int arrMem3;
    
    if (resp.equals(i)){ //preparing arrays for insert method
      
      System.out.print("What is your array 1 size?: "); //prompting user for array 1 size
      int int1Size= MyScanner.nextInt();
       int input1 []= new int [int1Size]; //allocating array 1 size
 
        for(int k=0; k<input1.length; k++){ //iterating for user to input values into array 1
            System.out.print("Enter the value of member " + k +":");
             arrMem1 = MyScanner.nextInt();
              input1[k]= arrMem1;
    }
      
       System.out.print("What is your array 2 size?: ");//prompting user for Array 2 size
          int int2Size= MyScanner.nextInt();
          int input2 []= new int [int2Size];//allocating array 2 size
      
        for(int k=0; k<input2.length; k++){ //iterating user to input values into array 2
            System.out.print("Enter the value of member" + k +":");
            arrMem2 = MyScanner.nextInt();
            input2[k]= arrMem2;
        }
        insert(input1, input2); //sending arrays to insert method
    }
    else if(resp.equals(s)){ //preparing arrays for shorten method
     System.out.print("What is your array size?: "); //prompting user for array size
        int arrSize= MyScanner.nextInt();
        int shortArr []= new int [arrSize]; //allocating array size
      
      for(int k=0; k<shortArr.length; k++){ //iterating user to input values into array
           System.out.print("Enter the value of member" + k +":");
           arrMem3 = MyScanner.nextInt();
           shortArr[k]= arrMem3;
      }
      
      System.out.print("Which index would you like to check?: "); //requesting user to input an index check
      int index= MyScanner.nextInt();
      
      shorten(shortArr, index); //sending array and index to shorten method
    }
  }
  
  public static void insert(int[] in1, int[] in2){
    
   // int newSize = in1.length+in2.length;
    int numSwitch = (int) (Math.random() * in1.length); 
    for(int i=0; i<in1.length; i++){//itteration of combining arrays
      if (i<numSwitch){
      System.out.print(in1[i]+ " ");
      }
       else if(i == numSwitch ){ //inputting the second array into the first array bewteen the random generated number 
          //and the random generated number plus the second arrays' length
         for(int j = 0; j < in2.length; j++) {
           System.out.print(in2[j] + " ");
         }    
      }
      else if (i > numSwitch){
      System.out.print(in1[i]+ " ");
      } 
  } 
    System.out.println();
  }
  
  public static void shorten(int[] in1, int index){
    if( index> in1.length){
      for(int i=0; i<in1.length; i++){
        System.out.print(in1[i]);
      }
    }
    else if(in1.length>= index){
      for(int j=0; j<in1.length; j++){
        if(j < index){
          System.out.print(in1[j]);
        }
        else if( j == index){
          System.out.print("");
        }
        else if(j > index){
          System.out.print(in1[j]);
        }
      }
    }
  }
  public static void print(int[] Array){
    System.out.println("Your array is:");
    for (int i=0; i<Array.length; i++){
      System.out.print(Array);
    }
    
  }
  
}