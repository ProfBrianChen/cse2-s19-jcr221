//Juan Rodriguez
//2/9/2019
//Homework 2

public class Arithmetic{
  public static void main(String[] args){
    //Number of pairs of Pants
    int numPants = 3;
    //cost per pair of pants
    double pantsPrice = 34.98;
    
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    
    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;
    
    //the tax rate
    double paSalesTax = 0.06;
    
    // declaring Total cost variables of pants, sweatshirts and belts
    double totalCostOfPants;
    double totalCostOfSweatshirts; 
    double totalCostOfBelts;
    double totalCostOfBeltsntax; 
    double totalCostOfSweatshirtsntax;
    double totalCostOfPantsntax ;
    
    // Assigning variables
    totalCostOfPants= pantsPrice*numPants;
    totalCostOfSweatshirts= shirtPrice*numShirts;
    totalCostOfBelts= numBelts*beltCost;
    
    //Sales tax of each clothing
    totalCostOfSweatshirtsntax= totalCostOfSweatshirts*paSalesTax;
    totalCostOfPantsntax= totalCostOfPants*paSalesTax;
    totalCostOfBeltsntax= totalCostOfBelts*paSalesTax;
    
    //Total cost of purchase BEFORE taxes
    double totalCostwotax= totalCostOfBelts+totalCostOfPants+totalCostOfSweatshirts;
    
    //total Sales taxes
    double totalTax= totalCostOfBeltsntax+totalCostOfSweatshirtsntax+totalCostOfPantsntax;
    
    //total transaction
    double totalTransaction= totalTax+totalCostwotax;
    
    //Print Total Cost before sales tax, the total tax, and the total cost of purchase to terminal
    System.out.println(totalCostwotax);
    System.out.println(totalTax);
    System.out.println(totalTransaction);
  }
}