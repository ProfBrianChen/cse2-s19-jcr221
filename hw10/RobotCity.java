import java.util.Random;
import java.util.Formatter;

public class RobotCity{
  public static void main(String args[]){
    Random RanGen = new Random();
    
    int width= RanGen.nextInt(15);
    int height= RanGen.nextInt(15);
    for(int i=0; i<5; i++){
    if(width<10){
      width=RanGen.nextInt(15);
    }
    if(height<10){
      height=RanGen.nextInt(15);
      }
      
    }
      
    int k= RanGen.nextInt(99);
      int[][] City= buildCity(width, height);
        display(City);
      
    int[][]Robots = Invade(City, k, width, height);
    for(int i =0; i<Robots.length; i++){
      for(int j=0; j<Robots[i].length; j++){
        if(City[i][j]==k){
          System.out.print("X");
        }
        else{
        System.out.printf(Robots[i][j] + " ");
        }
      }
      System.out.println(" ");
    }  
  
    
    for(int l=0; l<5; l++){
    int[][] UpdateCity= update(Robots,k);
    for(int i =0; i<Robots.length; i++){
      for(int j=0; j<Robots[i].length; j++){
        if(City[i][j]==k){
          System.out.print("X");
        }
        else{
        System.out.printf(Robots[i][j] + "   /n");
        }
      }
      System.out.println(" ");
    }  
    }
  }

    public static int[][] buildCity(int width, int height){
    Random RanGen = new Random();
     int [][] City = new int[height][width];
    for(int i=0; i< City.length; i++){
      for(int j=0; j<City[i].length; j++){
        City[i][j]= RanGen.nextInt(999);
        if(City[i][j]<100){
          City[i][j]=RanGen.nextInt(999);        
        }
      }
    }
      return City;
  }
  
 
  public static int[][] Invade(int[][] City, int k, int x, int y){
    Random RanGen = new Random();
    for(int i=0; i<k; i++){
      City[x][y]= k;
    }
    return City; 
  }
  
  
  public static int [][] update(int[][] Robots, int k){
    for(int i=0; i<Robots.length; i++){
      for(int j=0; j<Robots[i].length; j++){
        if(Robots[i][j]==k){
          if(k ==Robots.length-1){
            Robots[i][j] = Robots[i][0];
            Robots[i][0]=k;
          }
          else{
            Robots[i][j] = Robots[i][j+1];
            Robots[i][j+1]=k;
          }
        }
      }
    }
    return Robots;
  }
  
  
  public static void display(int[][] City){
    for(int i =0; i<City.length; i++){
      for(int j=0; j<City[i].length; j++){
        System.out.printf(City[i][j] + " ");
        
      }
      System.out.println(" ");
    }
  }
}