import java.util.Random;
import java.util.Arrays;


public class Straight{
  public static void main(String args[]){
   // Scanner MyScanner = new Scanner(System.in);
    int[] deck = new int[52];
    int []cards = DeckGen(deck);
    int [] shuffCards = shuffCards1(cards);
    int[] hand = draw(shuffCards);
    Arrays.sort(hand);
    order(hand);
  }
  
  public static int[] DeckGen(int[] deck){
    int counter = 1;
    int counter1 = 1;
    int counter2 =1;
    int counter3=1;
    
    for(int i=0; i<13; i++){
      deck[i]= counter;
      counter++;
    }
     for(int j=14; j<26; j++){
      deck[j]= counter1;
      counter1++;
    }
    for(int k=27; k<39; k++){
      deck[k]= counter1;
      counter2++;
    }
    for(int l=40; l<52; l++){
      deck[l]= counter1;
      counter3++;
    }
    return(deck);
  }
  
  public static int[] shuffCards1(int[] cards){
    Random RanGen = new Random();
      for(int i=0; i<cards.length; i++){
        int target = RanGen.nextInt(52);
        int temp = cards[target];
        cards[target]= cards[i];
        cards[i]= temp;
    } 
    return cards;
    
  }
  
  public static int[] draw(int[] shuffCards){
    Random RanGen = new Random();
    int [] hand = new int[5];
    for(int i=0; i<hand.length; i++){
      int mem = RanGen.nextInt(shuffCards.length);
      hand[i] = shuffCards[mem];
    }
    return hand;
  }
  
  public static void order(int[] hand){
        int l=0;
        int k=0;
        int t=0;
        int counter=0;
        
    for(int j=0; j<1000000; j++)
        for(int i=0; i<hand.length-1; i++){
          if(hand[counter]==hand[i+1]-1){
            l++;
            counter++;
          }
        }
    if(l==3){
      k++;
    }
    else{
     t++;
    }
      double straightPercentage = (k/t);
    System.out.println("The percentage of getting a straight out of one million is " + straightPercentage);
  }
    
    
  }
  
