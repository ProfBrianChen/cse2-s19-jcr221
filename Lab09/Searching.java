import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;

public class Searching{
  public static void main(String args[]){
    
    Scanner MyScanner = new Scanner(System.in);

    System.out.print("What is the array size? ");
    int arrSize = MyScanner.nextInt();
    //Gen(arrSize);
    
    System.out.print("Do you want to use a Linear search or a Binary Search:  ");
    String resp = MyScanner.next();
    
    String L = "Linear";
    String B = "Binary";
    
    
   if (resp.equals(L)){
      System.out.print("What Number do you want to search for?: ");
      int index1 = MyScanner.nextInt();
      
    //  System.out.print(Gen(arrSize) + "\n");
      System.out.print("\n"+"found at index  " + Linear(Gen(arrSize), index1) + "\n" );
    }
    
    else if(resp.equals(B)){
      System.out.print("What Number do you want to search for?: ");
      int index2 = MyScanner.nextInt();
      
     // System.out.print(Gen(arrSize) + "\n");
      int[] Array= Gen(arrSize);
      Arrays.sort(Array);
     
     
      System.out.print( "\n" + "found at index  " + Binary(Array, index2) + "\n");
   
   
  }
    
  }
  
  public static int[] Gen(int arrSize){
    
    Random RanGen= new Random();
    int [] Array= new int[arrSize];
  
    System.out.print("Array is: ");
    for (int i=0; i<Array.length; i++){
      Array[i] = RanGen.nextInt(Array.length); 
      System.out.print(Array[i] +" ");
    }
     System.out.println(" ");
    return Array;
   
  }
  
  public static int Linear(int[] Array, int index1){
    for(int i=0; i<Array.length; i++){
      if (index1 == Array[i]){
        return i;
      }
    }
    return -1;
  }
                       
   public static int Binary(int[] Array, int index2){
     
     
     int upper = Array[Array.length-1];
     int lower = Array[0];
     int midd = (upper+lower)/2;
     if(midd%2 == 0){
       midd = (upper+lower+1)/2; 
       
     while(lower<= upper){
      
       if (Array[midd] == index2){
         return midd;
       }
       else if(index2<Array[midd]){
         upper = midd-1;
       
       }
       else if(index2>Array[midd]){
         lower = midd+1;
       }
     }
       
       
    
      
     }
  return -1;
   }
}
                     
  
  

