//Juan Rodriguez  
//  2/14/2019
//Lab02

//This code is meant to give the time elapsed and the number of rotations of the front wheel during a given time frame
public class Cyclometer{
  public static void main(String[] args){
    
    //declaring time and trip variables
    int secsTrip1= 480;
    int secsTrip2= 3220;
    int countsTrip1= 1561;
    int countsTrip2= 9037;
    
    //assigning useful constants 
    double wheelDiameter =27.0;
    double PI= 3.14159;
    int feetPerMile= 5280;
    int inchesPerFoot= 12;
    int secondsPerFoot= 12;
    int secondsPerMinute= 60;
    double distanceTrip1;
    double distanceTrip2;
    double totaldistance;
    
    //printing the converted number of minutes from seconds
    System.out.println("Trip 1 took" + (secsTrip1/secondsPerMinute) +
                       "minutes and had " +countsTrip1+"counts.");
    System.out.println("trip 2 took" + (secsTrip2/secondsPerMinute) + 
                       "minutes and had " + countsTrip2 + "counts.");
    //The abive just calculated and printed the amount of time each trip took
    //storing the calculated time per trip
    int minTrip1= 8;
    int minTrip2= 53;
    //Calculating the distance of trip 1
    distanceTrip1 = (((countsTrip1*wheelDiameter*PI)/inchesPerFoot)/feetPerMile);
    distanceTrip2=(((countsTrip2*wheelDiameter*PI)/inchesPerFoot)/feetPerMile);
    //Calculating total distance
    totaldistance=distanceTrip1+distanceTrip2;
    //printing out the distances
    System.out.println("Trip 1 was" + distanceTrip1+ "miles");
    System.out.println("Trip 2 was" + distanceTrip2+ "miles");
    System.out.println("the total distance was"+ totaldistance+ "miles");
    
    
  }
}