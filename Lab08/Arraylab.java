import java.util.Arrays;
import java.util.Random; //imports random generator

public class Arraylab{ //class
public static void main(String args[]){ //main mehtod
  
  Random RanGen= new Random(); //random generator import
  
  int ranSize= RanGen.nextInt(100); //setting array size to random generator
  while(ranSize<50){ //checking for if the size is less than 50 
    ranSize= RanGen.nextInt(100);
  }
  
  int[] randomNumbers; //declaring array
  randomNumbers = new int[ranSize]; //allocating array
  
  System.out.println(" Array size: " + ranSize); //printing array size
  
  
  System.out.print("Entries are: ");
  for ( int i=0; i < ranSize; i++){ //setting the entry values
    int ranInt= RanGen.nextInt(100);
    randomNumbers[i]= ranInt;
    System.out.print(randomNumbers[i] + "  ");
  }
  System.out.println();
  System.out.println();
  System.out.println("The range of the array is: " + getRange(randomNumbers));
  System.out.println(" The mean is: " + getMean(randomNumbers));
  System.out.println("The Standard Deviation is: " + getStdDev(randomNumbers, getMean(randomNumbers)));
}
 public static int getRange(int[] Ar){
   Arrays.sort(Ar);
   System.out.println("The array sorted is: " );
   for(int i=0; i<Ar.length; i++){
     System.out.print(Ar[i]+"  ");
     
   }
   
   int range = Ar[Ar.length-1]-Ar[0];
   
   return range;
    
  }  
 public static double getMean(int[] Ar){
   double sum=0.0;
   for (int i=0; i<Ar.length; i++){
    sum= sum+Ar[i];
   }
   double Avr=sum/(Ar.length-1);
   return Avr;
 }
  
  public static double getStdDev(int[] Ar, double Mean){
   double sum1 = 0.0;
    double [] num = new double [Ar.length];
    for(int i=0; i<Ar.length; i++){
      num[i]= (Ar[i]-Mean)*(Ar[i]-Mean);
      sum1 = sum1 + num[i];
    }
   double Std= Math.sqrt(sum1/Ar.length);
    return Std;
  }
}