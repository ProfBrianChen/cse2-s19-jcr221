/////////////
//Homework #1 Welcome Class
//Juan Rodriguez
//1/29/2019

public class WelcomeClass{
  public static void main(String args[]){
    ///prints homework design
    System.out.println("    -----------");
    System.out.println("    | WELCOME |");
    System.out.println("    -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-J--C--R--2--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /  ");
    System.out.println("  v  v  v  v  v  v");
  }
}


