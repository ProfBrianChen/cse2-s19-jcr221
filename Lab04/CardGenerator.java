// Juan Rodriguez
// 2/15/2019
//Lab04

public class CardGenerator{
  public static void main(String args[]){
    //expliciting casting random number to integer
    //finding the randomNumber between 1-54
    int c1 = (int)(Math.random()*52);
    switch (c1){
      case 1:
          System.out.println("Your card is the Ace Diamond");
        break;
      case 2:
            System.out.println("Your card is the 2 of Diamonds");
        break;
      case 3:
            System.out.println("Your card is the 3 of Diamonds");
        break;
      case 4:
            System.out.println("Your card is the 4 of Diamonds");
        break;
      case 5:
            System.out.println("Your card is the 5 of Diamonds");
        break;
      case 6:
            System.out.println("Your card is the 6 of Diamonds");
        break;
      case 7:
            System.out.println("Your card is the 7 of Diamonds");
        break;
      case 8:
            System.out.println("Your card is the 8 of Diamonds");
        break;
      case 9:
            System.out.println("Your card is the 9 of Diamonds");
        break;
      case 10:
            System.out.println("Your card is the 10 of Diamonds");
        break;
      case 11:
            System.out.println("Your card is the J of Diamonds");
        break;
      case 12:
            System.out.println("Your card is the Q of Diamonds");
        break;
      case 13:
            System.out.println("Your card is the K of Diamonds");
        break;
      case 14:
            System.out.println("Your Card is Ace of Clubs");
        break;
      case 15:
            System.out.println("Your Card is 2 of Clubs");
        break;
      case 16 :
            System.out.println("Your Card is 3 of Clubs");
        break;
      case 17 :
            System.out.println("Your card is 4 of Clubs");
        break;
      case 18 :
            System.out.println("Your card is 5 of Clubs");
        break;
      case 19 :
            System.out.println("Your card is 6 of Clubs");
        break;
      case 20 :
            System.out.println("Your card is 7 of Clubs");
        break;
      case 21 :
            System.out.println("Your Card is 8 of Clubs ");
        break;
      case 22 :
            System.out.println("Your Card is 9 of Clubs");
        break;
      case 23 :
            System.out.println("Your card is 10 of Clubs");
        break;
      case 24 :
            System.out.println("Your card is J of Clubs");
        break;
      case 25 :
            System.out.println("Your card is Q of Clubs");
        break;
      case 26 :
            System.out.println("Your card is K of Clubs");
        break;
      case 27 :
            System.out.println("Your card is A of Hearts");
        break;
      case 28 :
            System.out.println("Your card is 2 of Hearts ");
        break;
      case 29 :
            System.out.println("Your card is 3 of Hearts");
        break;
      case 30 :
            System.out.println("Your Card is 4 of Hearts");
        break;
      case 31 :
            System.out.println("Your Card is 5 of Hearts");
        break;
      case 32 :
            System.out.println("Your card is 6 of Hearts");
        break;
      case 33 :
            System.out.println("Your card is 7 of Hearts");
        break;
      case 34 :
            System.out.println("Your card is 8 of Hearts");
        break;
      case 35 :
            System.out.println("Your card is 9 of Hearts");
        break;
      case 36 :
            System.out.println("Your card is 10 of hearts");
        break;
      case 37 :
            System.out.println("Your card is J of Hearts");
        break;
      case 38 :
            System.out.println("Your card is Q of Hearts");
        break;
      case 39 :
            System.out.println("Your card is K of Hearts");
        break;
      case 40 :
            System.out.println("Your card is Ace of Spades");
        break;
      case 41 :
            System.out.println("Your card is 2 of Spades");
        break;
      case 42 :
            System.out.println("Your card is 3 of Spades");
        break;
      case 43 :
            System.out.println("Your card is 4 of Spades");
        break;
      case 44 :
            System.out.println("Your card is 5 of Spades");
        break;
      case 45 :
            System.out.println("Your card is 6 of Spades");
        break;
      case 46 :
            System.out.println("Your card is 7 of Spades");
        break;
      case 47 :
            System.out.println("Your card is 8 of Spades");
        break;
      case 48 :
            System.out.println("Your card is 9 of Spades");
        break;
      case 49 :
            System.out.println("Your card is 10 of Spades");
        break;
      case 50 :
            System.out.println("Your card is J of Spades");
        break;
      case 51 :
            System.out.println("Your card is Q of Spades");
        break;
      case 52 :
            System.out.println("Your card is K of Spades");
        break;
      
      default:System.out.println("no card has been drawn");
       
    }
    
     
  }
}