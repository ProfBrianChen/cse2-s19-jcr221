import java.util.Random;

public class Letters{
  
  public static char[] getAtoM( char[] AtoM){

    char AlphAM[];
    int count = 0;
    AlphAM = new char[51];
    for(int i=0; i<AtoM.length; i++ ){
      if ((AtoM[i]-'a'<=12 && AtoM[i]-'a'>=0) || (AtoM[i]-'A'<=12 && AtoM[i]-'A'>=0)){
        //AlphAM= new char[i+1];
        AlphAM[count]=AtoM[i];
        count++;
      }      
    }
    if (count < 26) {
    char res[] = new char[count];
    for(int i = 0; i < res.length; i++) {
      res[i] = AlphAM[i];
    }
      return res;
    }
    return AlphAM;
  }

  public static char[] getNtoZ( char[] NtoZ){

    char AlphNZ[];
    int count = 0;
    AlphNZ = new char[51];
    for(int i=0; i<NtoZ.length; i++ ){
      if ((NtoZ[i]-'n'<=12 && NtoZ[i]-'n'>=0) || (NtoZ[i]-'N'<=12 && NtoZ[i]-'N'>=0)){
        //AlphAM= new char[i+1];
        AlphNZ[count]=NtoZ[i];
        count++;
      }      
    }
    if (count < 26) {
    char res2[] = new char[count];
    for(int i = 0; i < res2.length; i++) {
      res2[i] = AlphNZ[i];
    }
      return res2;
    }
    return AlphNZ;
  }


  public static void main(String args[]){
    
    Random RanGen= new Random();
    int ranSize= RanGen.nextInt(51);
    char[] result1 = new char[0];
    char[] result2 = new char[0];
    char[] Alph;
    Alph = new char[ranSize];
    System.out.print("Random character array: ");
    for (int i=0; i<ranSize; i++){
      switch(i){
        case 0:
          Alph[i]='A';
          break; 
        case 1:
          Alph[i]='z';
          break; 
        case 2:
          Alph[i]='B';
          break; 
        case 3:
          Alph[i]='y';
          break; 
        case 4:
          Alph[i]='C';
          break; 
        case 5:
          Alph[i]='x';
          break; 
        case 6:
          Alph[i]='D';
          break; 
        case 7:
          Alph[i]='w';
          break; 
        case 8:
          Alph[i]='E';
          break; 
        case 9:
          Alph[i]='v';
          break; 
        case 10:
          Alph[i]='F';
          break; 
        case 11:
          Alph[i]='u';
          break; 
        case 12:
          Alph[i]='G';
          break; 
        case 13:
          Alph[i]='t';
          break; 
        case 14:
          Alph[i]='H';
          break; 
        case 15:
          Alph[i]='s';
          break; 
        case 16:
          Alph[i]='I';
          break; 
        case 17:
          Alph[i]='r';
          break; 
        case 18:
          Alph[i]='J';
          break; 
        case 19:
          Alph[i]='q';
          break; 
        case 20:
          Alph[i]='K';
          break; 
        case 21:
          Alph[i]='p';
          break; 
        case 22:
          Alph[i]='L';
          break; 
        case 23:
          Alph[i]='o';
          break; 
        case 24:
          Alph[i]='M';
          break; 
        case 25:
          Alph[i]='n';
          break; 
        case 26:
          Alph[i]='N';
          break; 
        case 27:
          Alph[i]='m';
          break; 
        case 28:
          Alph[i]='O';
          break; 
        case 29:
          Alph[i]='l';
          break; 
        case 30:
          Alph[i]='P';
          break; 
        case 31:
          Alph[i]='k';
          break; 
        case 32:
          Alph[i]='Q';
          break; 
        case 33:
          Alph[i]='j';
          break; 
        case 34:
          Alph[i]='R';
          break; 
        case 35:
          Alph[i]='i';
          break; 
        case 36:
          Alph[i]='S';
          break; 
        case 37:
          Alph[i]='h';
          break; 
        case 38:
          Alph[i]='T';
          break; 
        case 39:
          Alph[i]='g';
          break; 
        case 40:
          Alph[i]='U';
          break; 
        case 41:
          Alph[i]='f';
          break; 
        case 42:
          Alph[i]='V';
          break; 
        case 43:
          Alph[i]='e';
          break; 
        case 44:
          Alph[i]='W';
          break; 
        case 45:
          Alph[i]='d';
          break; 
        case 46:
          Alph[i]='X';
          break; 
        case 47:
          Alph[i]='c';
          break; 
        case 48:
          Alph[i]='Y';
          break; 
        case 49:
          Alph[i]='b';
          break; 
          case 50:
          Alph[i]='Z';
          break; 
        default:
          Alph[i]='a';
          break; 
      }
      System.out.print(Alph[i]+"     " );
      //getAtoM(Alph);
      
       // getNtoZ(Alph[i]);
     // System.out.print("The charaters from A to M are: " + result[i]);
    } 
    result1=getAtoM(Alph);
    System.out.print("\nThe charaters from A to M are: ");
    for (int j=0; j<result1.length; j++){
    System.out.print(result1[j]+"     ");
  }
    
     result2=getNtoZ(Alph);
    System.out.print("\nThe charaters from N to Z are: ");
    for (int x=0; x<result2.length; x++){
    System.out.print(result2[x]+"     ");
  }
  }
}

