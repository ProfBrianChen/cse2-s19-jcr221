//Juan Rodriguez
//  2/12/2019
//Homework 3

//Importing the input Scanner
import java.util.Scanner;

public class BoxVolume{
  public static void main(String[] args) {
    //Constructing the instance for user inputs
   Scanner myScanner = new Scanner (System.in);
   //Declaring Variables
    int Width;
    int Length;
    int Height;
    int Volume;
    //Print statements requesting for input variables and assigning the input vlues to variables
    System.out.print("Enter width(no decimals)= ");
      Width= myScanner.nextInt();
    System.out.print("Enter length(no decimals)= ");
       Length = myScanner.nextInt();
    System.out.print("Enter Height(no decimals)= ");
        Height = myScanner.nextInt();
    //volume equation
    Volume= Length*Height*Width;
    //print volume results
    System.out.println("The volume inside the box is:" + Volume);
    
  }
}