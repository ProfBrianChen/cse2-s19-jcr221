//Juan Rodriguez
//   2/12/2019
// Homework 3

//Improting Scanner
import java.util.Scanner;

//Main class
public class Convert{
    public static void main(String[] args){
      //Constructing the instance of Scanner
      Scanner myScanner= new Scanner(System.in);
       //Storing data that is from user input
       System.out.print("Enter distance in meters in form xx.xx:");
      //Declaring Variables and types
      double DistanceInMeters;
      double InchesInMeterConvert;
      double DistanceInInches;
      //Accepting User Input 
      DistanceInMeters = myScanner.nextDouble();
     //Assigning the converion factor to the conversion variable
      InchesInMeterConvert=39.37;
      //converting the meters into Inches
      DistanceInInches= DistanceInMeters*InchesInMeterConvert;
      //print the two distances to the terminal
      System.out.println(DistanceInMeters + " meters is " + DistanceInInches + " inches");
    }
}

