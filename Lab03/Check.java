//Juan Rodriguez
//2/8/2019

//Code helps group of friends split a bill, add tips and how much each person will pay.
//In order to have user inputs work throughout code Scanner must be imported 
import java.util.Scanner;

public class Check{
  //This is the main method that java requires for every Java Program
  public static void main (String[] args){
  //The instance that must be used for a user's input to be declared and used as a standard input
    Scanner myScanner = new Scanner( System.in );
    //Asks user to input the original cost of the check
    System.out.print("Enter the original cost of the check in the form xx.xx:");
    //The next line accepts the user's input
    double checkCost = myScanner.nextDouble();
    //prints out a prompt instructing the user to enter a tip
    System.out.print("Enter the percentage tip that you wish to pay as a whole number(in the form xx):");
   //takes tip and accepts it into code
    double tipPercent = myScanner.nextDouble();
    tipPercent /=100; //This allows for the percentage to be converted into a decimal
      double totalCost;
    double costPerPerson;
    int dollars,    //whole dollar amount of cost
          dimes, pennies; //used for storing digits
              //to the right of the decimal point
              //for the cost$
    //prints out prompt for how many people are splitting bill
    System.out.print("Enter the number of people who is splitting the bill: ");
    //stores number of people splitting bill as integer
    int numPeople = myScanner.nextInt();
    //calculating the total cost
    totalCost = checkCost * (1+tipPercent);
    //finding the total cost per person
    costPerPerson = totalCost / numPeople;
    //explicit casting the cost per person to the dollars variable
    dollars=(int)costPerPerson;
    //explicit casting the cost per person and converting the cost into dimes
    dimes = (int)(costPerPerson*100)%10;
    //explicit casting the cost per person and converting the cost into pennies
    pennies=(int)(costPerPerson*100)%10;
    //printing the cost of each person in the group
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes+pennies);
  }
                  }