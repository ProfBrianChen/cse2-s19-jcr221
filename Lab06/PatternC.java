//Juan Rodriguez
//  3/12/2019

import java.util.Scanner;
public class PatternC{
  public static void main(String args[]){
    Scanner MyScanner= new Scanner(System.in);
    System.out.print("What is the length(Number of rows from 1-10): ");
    
    while(!MyScanner.hasNextInt()){
           System.out.print("Invalid Entry, Enter the length as an integer: ");
     String junkWord = MyScanner.next();
    }
      int n= MyScanner.nextInt();
      
    while(n<1 || n>10){
       System.out.print("Invalid Entry, enter an integer from 1-10: ");
      n= MyScanner.nextInt();
    }
    
    for(int nr = 1; nr <= n; nr++){
      //loop creates right allignment
      for(int ns = n; ns >= nr; ns--){
        System.out.print(" ");
      }
      //loop prints numbers in reverse order
      for(int nc = nr; nc>= 1; nc--){
        System.out.print(nc);
      }
      //prints a line between every loop
       System.out.println();
    }
   
    
  }
}  