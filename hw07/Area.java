//Juan Rodriguez
// 3/23/2019
// Homework 7 prt1

import java.util.Scanner;//imports scanner
public class Area{
public static void shapeCheck(String shape ){//Method design to check what shape's area is needed
  
  //declaring shape variable rectangle, triangle or circle
  String r= "rectangle"; 
  String t="triangle";
  String c="circle";
  double b;
  double rad;
  double l;
  double h;
  double w;
  Scanner MyScanner = new Scanner(System.in); //Declaring Scanner
  
  if (shape.equals(r)){//checks if rectangle was inputed for shape variable
   
    System.out.print("Input width(as double): " );// prompts user for width input as double
 while(!MyScanner.hasNextDouble()){
     System.out.print("Invalid Entry, Enter the width as a double: ");
     String junkWord = MyScanner.nextLine();
    }
    //User's correct input stored
    w = MyScanner.nextDouble();
   
    
    System.out.print("Input Length(as double): ");//promts user for length as a double
  while(!MyScanner.hasNextDouble()){
     System.out.print("Invalid Entry, Enter the length as a double: ");
     String junkWord = MyScanner.nextLine();
    }
    //User's correct input stored
   l=MyScanner.nextDouble();
  
    rectArea(w,l);
   //System.out.print("Rectangular area is: " +  rectArea(w,l));//sends variables w and l to rectArea method
  }
  
  else if(shape.equals(t)){//checks if triangle was input for shape variable
    
   System.out.print("Input base(as double): " );// prompts user for base input as double
 while(!MyScanner.hasNextDouble()){
     System.out.print("Invalid Entry, Enter the base as a double: ");
     String junkWord = MyScanner.nextLine();
    }
    //User's correct input stored
    b = MyScanner.nextDouble();
   
    
    System.out.print("Input Height(as double): ");//promts user for height as a double
  while(!MyScanner.hasNextDouble()){
     System.out.print("Invalid Entry, Enter the height as a double: ");
     String junkWord = MyScanner.nextLine();
    }
    //User's correct input stored
   h=MyScanner.nextDouble();
  
    triArea(b,h);
   // System.out.print("Triangular area is: " + triArea(b,h));// sends variables to triArea Method
  }
  
 else if(shape.equals(c)) {//checks if shape variable is a circle
    System.out.print("Input width(as double): " );// prompts user for width input as double
 while(!MyScanner.hasNextDouble()){
     System.out.print("Invalid Entry, Enter the width as a double: ");
     String junkWord = MyScanner.nextLine();
    }
    //User's correct input stored
    rad = MyScanner.nextDouble();
    cirArea(rad);
 // System.out.print("Circular area is: " + cirArea(rad));//sends rad tof cirArea method
 }
  
  else{
    System.out.println("Incorrect Shape Input");
  }
}

public static double rectArea(double width, double height){//method for calculating rectangular area
  double rectArea=width*height;
  printM(rectArea);
  return rectArea;
  //return rectArea;
}

public static double triArea(double base, double height){//method for calculating Triangular area
  double TriArea=(base*height)/2;
  return printM(TriArea);
  //return TriArea;
}

public static double cirArea(double radius){//method for calculating circular area
  double pi=3.14;
  double sqR=Math.pow(radius, 2); //squaring radius
 double CirArea= pi*sqR;
  return printM (CirArea);
 // return CirArea;
}

public static double printM(double Area){
  System.out.println("Your area is: " + Area);
    return Area;

  
  
}

public static void main(String args[]){ //main method
   Scanner MyScanner = new Scanner(System.in); //Declaring Scanner
  System.out.print("Input whether the shape is a Rectangle, Triangle, or a circle(lower cases only): "); //prompts user for shape input
  
  while(!MyScanner.hasNext()){
    System.out.print("Invalid Entry!! Input rectangle, triangle or circle: ");
   String junkWord = MyScanner.nextLine(); 
  }
  String S = MyScanner.next();
  shapeCheck(S);
}
}
