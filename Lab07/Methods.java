//Juan Rodriguez
// 3/22/2019
//Lab07
//import class objects 
import java.util.Random; 
import java.util.Scanner;  


public class Methods{  
public static void main(String [] args){  
  
  //declare new random generator 
  Random randomGenerator = new Random(); 
  //initalize random generator 
  int randomInt = randomGenerator.nextInt(10); 
  adjectiveGenerator(randomInt);
  
  System.out.print ("The" + adjectiveGenerator(randomInt));
  randomInt = randomGenerator.nextInt(10); 
  
  System.out.print (adjectiveGenerator(randomInt));
  
  System.out.print(nounGenerator(randomInt)); 
  
  System.out.print (verbGenerator( randomInt) +" the");
  
  randomInt = randomGenerator.nextInt(10);   
  System.out.print (adjectiveGenerator(randomInt));
  
  randomInt = randomGenerator.nextInt(10);
  System.out.print(ObjGenerator(randomInt));  
  //print a new line space 
  System.out.println("\n"); 
  
  //for loop to print the sentences 
  for ( int i=0; i<=5; i++){
    randomInt = randomGenerator.nextInt(10); 
    adjectiveGenerator(randomInt); //calls the adjective method 
    System.out.print ("The" + adjectiveGenerator(randomInt));
    //create new random number
    randomInt = randomGenerator.nextInt(10); 
    System.out.print (adjectiveGenerator(randomInt)); //prints the random adjective generated
    
    System.out.print(nounGenerator(randomInt)); //print the random noun generated 
    System.out.print (verbGenerator ( randomInt) +" the");
    //create new random number 
    randomInt = randomGenerator.nextInt(10);   
    System.out.print (adjectiveGenerator(randomInt));
    //create new random number
    randomInt = randomGenerator.nextInt(10);
    System.out.print(ObjGenerator(randomInt));  
    System.out.println();
   }//end of for loop 
}//end of main method 

//method for adjectives 
public static String adjectiveGenerator(int randomInt){
String adjective = "";
 switch(randomInt){ 
   case 1: 
    return" Smart "; 
   
   case 2: 
     return " Kind "; 
   
   case 3: 
     return " funny "; 
   
   case 4: 
      return " sad "; 
    
     case 5: 
        return " awesome "; 
      
     case 6: 
       return " angrily "; 
  
     case 7: 
        return " clean "; 
      
     case 8: 
    return " new "; 
      
   default: 
   return  " Old "; 
   
 }//end of switch 
}//end of method

//method for nouns
 public static String nounGenerator(int randomInt){
String noun = "";
 switch(randomInt){ 
   case 1: 
    return" Olivia "; 
   
   case 2: 
     return " Professor "; 
   
   case 3: 
     return " Gio "; 
   
   case 4: 
      return " engineer "; 
    
     case 5: 
        return " friend "; 
      
     case 6: 
       return " Zach "; 
  
     case 7: 
        return " Will "; 
      
     case 8: 
    return " Liam ";  
      
   default: 
     return  " Arleene "; 
 }//end of switch
 
}//end of method 

public static String verbGenerator(int randomInt){
String verb = "";
 switch(randomInt){ 
   case 1: 
    return" counted "; 
   
   case 2: 
     return " played"; 
   
   case 3: 
     return " studied "; 
   
   case 4: 
      return " took "; 
    
     case 5: 
        return " read "; 
      
     case 6: 
       return " had "; 
  
     case 7: 
        return " went "; 
      
     case 8: 
    return " made "; 
       
   default: 
   return  "embodied "; 
   }//end of method 
}
public static String ObjGenerator(int randomInt){
String Obj = "";
 switch(randomInt){ 
   case 1: 
    return" Playstation "; 
   
   case 2: 
     return " dog "; 
   
   case 3: 
     return " Lizard "; 
   
   case 4: 
      return " Laptop "; 
    
     case 5: 
        return " trashcan "; 
      
     case 6: 
       return "book "; 
  
     case 7: 
        return " bottle "; 
      
     case 8: 
    return " bed "; 
       
   default: 
   return  "table "; 
   
 }//end of switch 
}//end of method
}//end of class